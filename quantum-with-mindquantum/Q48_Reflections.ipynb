{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"https://qworld.net\" target=\"_blank\" align=\"left\"><img src=\"../qworld/images/header.jpg\"  align=\"left\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Reflections"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_prepared by Abuzer Yakaryilmaz_\n",
    "\n",
    "_MindQuantum adaptation by Junyuan Zhou_\n",
    "<br><br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_We use certain tools from python library \"<b>matplotlib.pyplot</b>\" for drawing. Check the notebook [Python: Drawing](../python/Python06_Drawing.ipynb) for the list of these tools._"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We start with a very basic reflection."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Z-gate (operator) </h3>\n",
    "\n",
    "The indentity operator $ I = \\pmatrix{1 & 0 \\\\\\\\ 0 & 1} $ does not affect the computation.\n",
    "\n",
    "What about the following operator?\n",
    "\n",
    "$ Z = \\pmatrix{1 & 0 \\\\\\\\ 0 & -1} $.\n",
    "\n",
    "It is very similar to the identity operator.\n",
    "\n",
    "Consider the quantum state $ |u\\rangle = \\pmatrix{ \\frac{3}{5} \\\\\\\\ \\frac{4}{5} }  $.\n",
    "\n",
    "We calculate the new quantum state after applying `Z` to $ |u\\rangle $:\n",
    "\n",
    "$ |u'\\rangle = Z |u\\rangle = \\pmatrix{1 & 0 \\\\\\\\ 0 & -1}  \\pmatrix{ \\frac{3}{5} \\\\\\\\ \\frac{4}{5} } =  \\pmatrix{ \\frac{3}{5} \\\\\\\\ -\\frac{4}{5} } $.\n",
    "\n",
    "We draw both states below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from drawing import draw_axes, draw_unit_circle, draw_quantum_state, draw_qubit, draw_qubit_grover, show_plt\n",
    "\n",
    "draw_qubit()\n",
    "\n",
    "draw_quantum_state(3 / 5, 4 / 5, \"u\")\n",
    "\n",
    "draw_quantum_state(3 / 5, -4 / 5, \"u'\")\n",
    "\n",
    "show_plt()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we apply `Z` to the state $ |u'\\rangle $, we obtain the state $|u\\rangle$ again: $  \\pmatrix{1 & 0 \\\\\\\\ 0 & -1} \\pmatrix{ \\frac{3}{5} \\\\\\\\ -\\frac{4}{5} } =  \\pmatrix{ \\frac{3}{5} \\\\\\\\ \\frac{4}{5} }  $.\n",
    "\n",
    "It is easy to see that the operator `Z` is a reflection and its **line of reflection** is the $x$-axis.\n",
    "\n",
    "Remark that applying the same reflection twice on the unit circle does not make any change."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Task 1 </h3>\n",
    "\n",
    "Design a quantum circuit with 5 qubits as follows:\n",
    "- Apply `H` gate (Hadamard operator) to each qubit\n",
    "- Randomly pick a subset of qubits\n",
    "- Apply `Z` gate ($Z$ operator) to these randomly picked qubits, i.e.,\n",
    "\n",
    "    `circuit += Z.on(i)`\n",
    "    \n",
    "- Apply `H` gate to each qubit.\n",
    "- Measure each qubit.\n",
    "\n",
    "Execute your program 1000 times.\n",
    "\n",
    "Compare the outcomes of the qubits affected by `Z` gates and the outcomes of the qubits not affected by `Z` gates.\n",
    "\n",
    "Does applying `Z` gate change the outcome?\n",
    "\n",
    "If so, why?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import all necessary libraries\n",
    "from random import randrange\n",
    "from math import pi\n",
    "from mindquantum.core.circuit import Circuit\n",
    "from mindquantum.core.gates import H, Z, Measure\n",
    "\n",
    "\n",
    "######################\n",
    "# Enter your code here\n",
    "######################\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[click for our solution](Q48_Reflections_Solutions.ipynb#task1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Hadamard operator </h3>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Is `Hadamard` operator a reflection? If so, what is its line of reflection?\n",
    "\n",
    "Remember the following transitions.\n",
    "\n",
    "$H |0\\rangle = \\pmatrix{ \\frac{1}{\\sqrt{2}} & \\frac{1}{\\sqrt{2}} \\\\\\\\ \\frac{1}{\\sqrt{2}} & -\\frac{1}{\\sqrt{2}} } \\pmatrix{1 \\\\\\\\ 0} = \\pmatrix{\\frac{1}{\\sqrt{2}} \\\\\\\\ \\frac{1}{\\sqrt{2}} } = |+\\rangle\n",
    "\\quad$ and $\\quad H |+\\rangle = \\pmatrix{ \\frac{1}{\\sqrt{2}} & \\frac{1}{\\sqrt{2}} \\\\\\\\ \\frac{1}{\\sqrt{2}} & -\\frac{1}{\\sqrt{2}} } \\pmatrix{\\frac{1}{\\sqrt{2}} \\\\\\\\ \\frac{1}{\\sqrt{2}} } = \\pmatrix{1 \\\\\\\\ 0} = |0\\rangle$.\n",
    "\n",
    "$ H |1\\rangle = \\pmatrix{ \\frac{1}{\\sqrt{2}} & \\frac{1}{\\sqrt{2}} \\\\\\\\ \\frac{1}{\\sqrt{2}} & -\\frac{1}{\\sqrt{2}} } \\pmatrix{0 \\\\\\\\ 1} = \\pmatrix{\\frac{1}{\\sqrt{2}} \\\\\\\\ -\\frac{1}{\\sqrt{2}} } = |-\\rangle \\quad$ and $\\quad H |-\\rangle = \\pmatrix{ \\frac{1}{\\sqrt{2}} & \\frac{1}{\\sqrt{2}} \\\\\\\\ \\frac{1}{\\sqrt{2}} & -\\frac{1}{\\sqrt{2}} } \\pmatrix{\\frac{1}{\\sqrt{2}} \\\\\\\\ -\\frac{1}{\\sqrt{2}} } = \\pmatrix{0 \\\\\\\\ 1} = |1\\rangle $."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from drawing import draw_axes, draw_unit_circle, draw_quantum_state, draw_qubit, draw_qubit_grover, show_plt\n",
    "\n",
    "draw_qubit()\n",
    "\n",
    "sqrttwo = 2**0.5\n",
    "\n",
    "draw_quantum_state(1, 0, \"\")\n",
    "\n",
    "draw_quantum_state(1 / sqrttwo, 1 / sqrttwo, \"|+>\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from drawing import draw_axes, draw_unit_circle, draw_quantum_state, draw_qubit, draw_qubit_grover, show_plt\n",
    "\n",
    "draw_qubit()\n",
    "\n",
    "sqrttwo = 2**0.5\n",
    "\n",
    "draw_quantum_state(0, 1, \"\")\n",
    "\n",
    "draw_quantum_state(1 / sqrttwo, -1 / sqrttwo, \"|->\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Hadamard - geometrical interpretation </h3>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`Hadamard` operator is a reflection and its line of reflection is represented below.\n",
    "\n",
    "It is the line obtained by rotating $x$-axis with $ \\frac{\\pi}{8} $ radians in counter-clockwise direction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from drawing import draw_axes, draw_unit_circle, draw_quantum_state, draw_qubit, draw_qubit_grover, show_plt\n",
    "\n",
    "draw_qubit()\n",
    "\n",
    "sqrttwo = 2**0.5\n",
    "\n",
    "draw_quantum_state(1, 0, \"\")\n",
    "draw_quantum_state(1 / sqrttwo, 1 / sqrttwo, \"|+>\")\n",
    "\n",
    "draw_quantum_state(0, 1, \"\")\n",
    "draw_quantum_state(1 / sqrttwo, -1 / sqrttwo, \"|->\")\n",
    "\n",
    "# line of reflection for Hadamard\n",
    "from matplotlib.pyplot import arrow\n",
    "\n",
    "arrow(-1.109, -0.459, 2.218, 0.918, linestyle='dotted', color='red')\n",
    "\n",
    "# drawing the angle with |0>-axis\n",
    "from matplotlib.pyplot import gca, text\n",
    "from matplotlib.patches import Arc\n",
    "\n",
    "gca().add_patch(Arc((0, 0), 0.4, 0.4, angle=0, theta1=0, theta2=22.5))\n",
    "text(0.09, 0.015, '.', fontsize=30)\n",
    "text(0.25, 0.03, '\\u03C0/8')\n",
    "gca().add_patch(Arc((0, 0), 0.4, 0.4, angle=0, theta1=22.5, theta2=45))\n",
    "text(0.075, 0.065, '.', fontsize=30)\n",
    "text(0.21, 0.16, '\\u03C0/8')\n",
    "show_plt()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Task 2 </h3>\n",
    "\n",
    "Randomly create a quantum state and multiply it with Hadamard matrix to find its reflection.\n",
    "\n",
    "Draw both states.\n",
    "\n",
    "Repeat the task for a few times."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from drawing import draw_axes, draw_unit_circle, draw_quantum_state, draw_qubit, draw_qubit_grover, show_plt\n",
    "\n",
    "draw_qubit()\n",
    "\n",
    "# line of reflection for Hadamard\n",
    "from matplotlib.pyplot import arrow\n",
    "\n",
    "arrow(-1.109, -0.459, 2.218, 0.918, linestyle='dotted', color='red')\n",
    "\n",
    "#\n",
    "# your code is here\n",
    "#"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[click for our solution](Q48_Reflections_Solutions.ipynb#task2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Task 3 </h3>\n",
    "\n",
    "Find the matrix representing the reflection over the line $y=x$.\n",
    "\n",
    "<i>Hint: Think about the reflections of the points $ \\pmatrix{0 \\\\\\\\ 1} $, $ \\pmatrix{-1 \\\\\\\\ 0} $, and $ \\pmatrix{-\\frac{1}{\\sqrt{2}} \\\\\\\\ \\frac{1}{\\sqrt{2}}} $ over the line $y=x$.</i>\n",
    "\n",
    "Randomly create a quantum state and multiply it with this matrix to find its reflection over the line $y = x$.\n",
    "\n",
    "Draw both states.\n",
    "\n",
    "Repeat the task for a few times."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from drawing import draw_axes, draw_unit_circle, draw_quantum_state, draw_qubit, draw_qubit_grover, show_plt\n",
    "\n",
    "draw_qubit()\n",
    "\n",
    "# the line y=x\n",
    "from matplotlib.pyplot import arrow\n",
    "\n",
    "arrow(-1, -1, 2, 2, linestyle='dotted', color='red')\n",
    "\n",
    "#\n",
    "# your code is here\n",
    "#\n",
    "# draw_quantum_state(x,y,\"name\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[click for our solution](Q48_Reflections_Solutions.ipynb#task3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3>Reflection Operators</h3>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we have observed, the following operators are reflections on the unit circle."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b> `Z` operator:</b> $ Z = \\pmatrix{1 & 0 \\\\\\\\ 0 & -1} $. The line of reflection is $x$-axis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b> `NOT` operator:</b> $ X = \\pmatrix{ 0 & 1 \\\\\\\\ 1 & 0 } $. The line of reflection is $y=x$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b> `Hadamard` operator:</b> $ H = \\pmatrix{ \\frac{1}{\\sqrt{2}} & \\frac{1}{\\sqrt{2}} \\\\\\\\ \\frac{1}{\\sqrt{2}} & -\\frac{1}{\\sqrt{2}} } $. The line of reflection is $y= \\frac{\\sin(\\pi/8)}{\\cos(\\pi/8)} x$. \n",
    "\n",
    "It is the line passing through the origin making an angle $ \\pi/8 $ radians with $x$-axis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Arbitrary reflection operator:</b> Let $ \\theta $ be the angle of the line of reflection. Then, the martix form of reflection is represented as follows:\n",
    "\n",
    "$$ Ref(\\theta) = \\pmatrix{ \\cos(2\\theta) & \\sin(2\\theta) \\\\\\\\ \\sin(2\\theta) & -\\cos(2\\theta) } . $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "<h3> Extra: Task 4 </h3>\n",
    "\n",
    "The matrix forms of rotations and reflections are similar to each other.\n",
    "\n",
    "Represent $ Ref(\\theta) $ as a combination of a basic reflection operator (i.e., `X`, `H`, or `Z`) and rotation $ R(\\theta) $."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Extra: Task 5 </h3>\n",
    "\n",
    "Randomly pick the angle $\\theta$.\n",
    "\n",
    "Draw the line of reflection with the unit circle.\n",
    "\n",
    "Construct the corresponding reflection matrix.\n",
    "\n",
    "Randomly create a quantum state and multiply it with this matrix to find its reflection.\n",
    "\n",
    "Draw both states.\n",
    "\n",
    "Repeat the task for a few times."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from drawing import draw_axes, draw_unit_circle, draw_quantum_state, draw_qubit, draw_qubit_grover, show_plt\n",
    "\n",
    "draw_qubit()\n",
    "\n",
    "######################\n",
    "# Enter your code here\n",
    "######################\n",
    "\n",
    "######################\n",
    "# Line of reflection \n",
    "# from matplotlib.pyplot import arrow\n",
    "# arrow(x, y, dx, dy, linestyle='dotted',color='red')\n",
    "#\n",
    "#\n",
    "# draw_quantum_state(x, y, \"name\")\n",
    "######################"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
