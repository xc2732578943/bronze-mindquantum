{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"https://qworld.net\" target=\"_blank\" align=\"left\"><img src=\"../qworld/images/header.jpg\"  align=\"left\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Superposition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_prepared by Abuzer Yakaryilmaz_\n",
    "<br><br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is no classical counterpart of the concept \"superposition\".\n",
    "\n",
    "But, we can still use a classical analogy that might help us to give some intuitions.\n",
    "\n",
    "<h3> Probability distribution </h3>\n",
    "\n",
    "Suppose that Asja starts in $\\begin{pmatrix}1\\\\\\\\0\\end{pmatrix}$ and secretly applies the probabilistic operator $\\begin{pmatrix}0.3 & 0.6\\\\\\\\ 0.7 &0.4\\end{pmatrix}$.\n",
    "\n",
    "Because she applies her operator secretly, our information about her state is probabilistic, which is calculated as\n",
    "\n",
    "$$\n",
    "\\begin{pmatrix} 0.3 \\\\\\\\ 0.7 \\end{pmatrix} = \\begin{pmatrix}0.3 & 0.6\\\\\\\\ 0.7 &0.4\\end{pmatrix} \\begin{pmatrix}1\\\\\\\\0\\end{pmatrix}.\n",
    "$$\n",
    "\n",
    "Asja is either in state 0 or in state 1.\n",
    "\n",
    "However, from our point of view, Asja is in state 0 with probability $0.3$ and in state 1 with probability $0.7$.\n",
    "\n",
    "We can say that Asja is in a probability distribution of states 0 and 1, being in both states at the same time but with different weights.\n",
    "\n",
    "On the other hand, if we observe Asja's state, then our information about Asja becomes deterministic: either $\\begin{pmatrix}1\\\\\\\\0\\end{pmatrix}$ or $\\begin{pmatrix}0 \\\\\\\\ 1\\end{pmatrix}$.\n",
    "\n",
    "We can say that, after observing Asja's state, we update our information and the probabilistic state $\\begin{pmatrix} 0.3 \\\\\\\\ 0.7 \\end{pmatrix}$ becomes to either $\\begin{pmatrix}1\\\\\\\\0\\end{pmatrix}$ or $\\begin{pmatrix}0 \\\\\\\\ 1\\end{pmatrix}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> The third experiment </h3>\n",
    "\n",
    "Remember the following experiment. We trace it step by step by matrix-vector multiplication.\n",
    "\n",
    "<img src=\"./images/photon7.jpg\" width=\"65%\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b> The initial Step </b>\n",
    "\n",
    "The photon is in state $|v_0\\rangle = \\begin{pmatrix}1\\\\\\\\0\\end{pmatrix}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b> The first step </b>\n",
    "\n",
    "`Hadamard` is applied:\n",
    "\n",
    "$|v_0\\rangle = \\frac{1}{\\sqrt{2}}\\begin{pmatrix}1&1\\\\\\\\1&-1\\end{pmatrix} \\begin{pmatrix}1\\\\\\\\0\\end{pmatrix} = \\begin{pmatrix}\\frac{1}{\\sqrt{2}}\\\\\\\\\\frac{1}{\\sqrt{2}}\\end{pmatrix}$.\n",
    "\n",
    "At this point, the photon is in a <b>superposition</b> of state $|0\\rangle$ and state $|1\\rangle$, <u>being in both states with the amplitudes</u> $\\frac{1}{\\sqrt{2}}$ and $\\frac{1}{\\sqrt{2}}$, respectively.\n",
    "\n",
    "The state of photon is $|v_1\\rangle = \\begin{pmatrix}\\frac{1}{\\sqrt{2}}\\\\\\\\\\frac{1}{\\sqrt{2}}\\end{pmatrix}$, and we can also represent it as follows: \n",
    "$|v_1\\rangle =  \\frac{1}{\\sqrt{2}}|0\\rangle + \\frac{1}{\\sqrt{2}}|1\\rangle$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b> The second step </b>\n",
    "\n",
    "`Hadamard` is applied again:\n",
    "\n",
    "We write the effect of `Hadamard` on states $|0\\rangle$ and $|1\\rangle$ as follows:\n",
    "\n",
    "$H |0\\rangle = \\frac{1}{\\sqrt{2}} |0\\rangle + \\frac{1}{\\sqrt{2}} |1\\rangle$ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i>(These are the transition amplitudes of the first column.)</i>\n",
    "\n",
    "$H |1\\rangle = \\frac{1}{\\sqrt{2}} |0\\rangle - \\frac{1}{\\sqrt{2}} |1\\rangle$ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i>(These are the transition amplitudes of the second column.)</i>\n",
    "\n",
    "This representation helps us to see clearly why the state $|1\\rangle$ would disappear after applying the second `Hadamard`.\n",
    "\n",
    "Now, let's see the effect of `Hadamard` on the quantum state $|v_1\\rangle  =  \\frac{1}{\\sqrt{2}}|0\\rangle + \\frac{1}{\\sqrt{2}} |1\\rangle$:\n",
    "\n",
    "$|v_2\\rangle  = H|v_1\\rangle  = H \\left( \\frac{1}{\\sqrt{2}}|0\\rangle + \\frac{1}{\\sqrt{2}}|1\\rangle \\right) = \\frac{1}{\\sqrt{2}} H |0\\rangle + \\frac{1}{\\sqrt{2}} H|1\\rangle $\n",
    "\n",
    "We can replace $H|0\\rangle$ and $H|1\\rangle$ as described above. $|v_2\\rangle $ is formed by the summation of the following terms:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$~~~~~~~~ \\frac{1}{\\sqrt{2}} H |0\\rangle = $ <font color=\"green\">$\\frac{1}{2} |0\\rangle $</font> <font color=\"red\">$ + \\frac{1}{2}  |1\\rangle  $</font>\n",
    "\n",
    "$~~~~~~~~ \\frac{1}{\\sqrt{2}} H |1\\rangle = $ <font color=\"green\">$\\frac{1}{2}  |0\\rangle $</font> <font color=\"red\">$ - \\frac{1}{2}  |1\\rangle $</font>\n",
    "<br>\n",
    "<font size=\"+1\">$ \\mathbf{+}\\mbox{___________________} $</font>\n",
    "\n",
    "$ ~~ $ <font color=\"green\"> $\\left( \\frac{1}{2}+\\frac{1}{2} \\right) |0\\rangle $</font> + \n",
    "<font color=\"red\"> $\\left( \\frac{1}{2}-\\frac{1}{2} \\right)  |1\\rangle $ </font> $ = \\mathbf{|0\\rangle} $.\n",
    "\n",
    "<font color=\"green\">The amplitude of $|0\\rangle$ becomes 1, </font> <font color=\"red\"> but the amplitude of $|1\\rangle$ becomes 0 because of cancellation.</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The photon was in both states at the same time with <u>certain amplitudes</u>.\n",
    "\n",
    "After the second `Hadamard`, the \"outcomes\" are <u>interfered with each other</u>.\n",
    "\n",
    "The interference can be constructive or destructive.\n",
    "\n",
    "In our examples, <font color=\"green\"><b>the outcome $|0\\rangle$s are interfered constructively</b></font>, but <font color=\"red\"><b>the outcome $|1\\rangle$s are interfered destructively</b></font>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Observations </h3>\n",
    "\n",
    "<u>Probabilistic systems</u>: If there is a nonzero transition to a state, then it contributes to the probability of this state positively. \n",
    "\n",
    "<u>Quantum systems</u>: If there is a nonzero transition to a state, then we cannot know its contribution without knowing the other transitions to this state.\n",
    "\n",
    "If it is the only transition, then it contributes to the amplitude (and probability) of the state, and it does not matter whether the sign of the transition is positive or negative.\n",
    "\n",
    "If there is more than one transition, then depending on the summation of all transitions, we can determine whether a specific transition contributes or not.\n",
    "\n",
    "As a simple rule, if the final amplitude of the state and nonzero transition have the same sign, then it is a positive contribution; and, if they have the opposite signs, then it is a negative contribution."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Task 1 </h3>\n",
    "\n",
    "[on paper]\n",
    "\n",
    "Start in state $|u_0\\rangle =  |1\\rangle$.\n",
    "\n",
    "Apply `Hadamard` operator to $|u_0\\rangle$, i.e, find $|u_1\\rangle = H |u_0\\rangle$.\n",
    "\n",
    "Apply `Hadamard` operator to $|u_1\\rangle$, i.e, find $|u_2\\rangle = H |u_1\\rangle$.\n",
    "\n",
    "Observe the constructive and destructive interferences, when calculating $|u_2\\rangle$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Being in a superposition </h3>\n",
    "\n",
    "A quantum system can be in more than one state with nonzero amplitudes.\n",
    "\n",
    "Then, we say that our system is in a superposition of these states.\n",
    "\n",
    "When evolving from a superposition, the resulting transitions may affect each other constructively and destructively. \n",
    "\n",
    "This happens because of having opposite sign transition amplitudes. \n",
    "\n",
    "Otherwise, all nonzero transitions are added up to each other as in probabilistic systems."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
